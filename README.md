boinc-client
============

Docker container to run [BOINC client](http://boinc.berkeley.edu/).

### Usage

Run container:

    $ docker run -d --restart=always -p 31416:31416 \
        -e BOINC_GUI_RPC_PASSWORD="123" \
        -e BOINC_CMD_LINE_OPTIONS="--allow_remote_gui_rpc" \
        --name boinc hleroy/boinc-client

You can connect remotely using [BOINC Manager](https://boinc.berkeley.edu/wiki/BOINC_Manager).

## Parameters

| Parameter | Function |
| :--- | :--- |
| `-e BOINC_GUI_RPC_PASSWORD="123"` | The password that you need to use, when you connect to the BOINC client.  |
| `-e BOINC_CMD_LINE_OPTIONS="--allow_remote_gui_rpc"` | The `--allow_remote_gui_rpc` command-line option allows connecting to the client with any IP address. If you don't want that, you can remove this parameter, but you have to use the `-e BOINC_REMOTE_HOST="IP"`. |
| `-e BOINC_REMOTE_HOST="IP"` | (Optional) Replace the `IP` with your IP address. In this case you can connect to the client only from this IP. |
