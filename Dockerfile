FROM debian:bullseye-slim

LABEL maintainer="hleroy@hleroy.com"

# Global environment settings
ENV BOINC_GUI_RPC_PASSWORD="123" \
     BOINC_CMD_LINE_OPTIONS="" \
     BOINC_REMOTE_HOST="127.0.0.1" \
     DEBIAN_FRONTEND=noninteractive

# Install boinc-client
RUN apt-get update && apt-get install -q -y --no-install-recommends \
     boinc-client && \
     apt-get clean && \
     rm -rf /var/lib/apt/lists/*

# Copy files
COPY start-boinc.sh /usr/bin/

# Configure working direcotry
WORKDIR /var/lib/boinc

EXPOSE 31416

# Persist configuration and data
VOLUME ["/etc/boinc-client", "/var/lib/boinc-client"]

WORKDIR /var/lib/boinc-client

HEALTHCHECK CMD boinccmd --get_state

CMD ["/usr/bin/start-boinc.sh"]
